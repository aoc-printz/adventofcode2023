package challenges;

import common.AdventOfCodeBase;
import common.Part;


public class DayTemplate extends AdventOfCodeBase {

    public DayTemplate(String inputFilename, Part part) {
        super(inputFilename, part);
    }

    @Override
    public String run() {
        System.out.println("INPUT lines.size() = " + lines.size());

        if( isPart1()) {
            int answer = 0;

            for( String line : lines ) {
                System.out.println("line = " + line);

            }
            return ""+answer;
        }

        if( isPart2() ) {
            int answer = 0;

            for( String line : lines ) {
                System.out.println("line = " + line);

            }

            return ""+answer;
        }

        return "";
    }


}
