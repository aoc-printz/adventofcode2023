package challenges;

import common.AdventOfCodeBase;
import common.Part;
import org.apache.commons.lang3.StringUtils;

import java.util.*;


public class Day4 extends AdventOfCodeBase {

    public Day4(String inputFilename, Part part) {
        super(inputFilename, part);
    }

    @Override
    public String run() {
        System.out.println("INPUT lines.size() = " + lines.size());

        if( isPart1()) {
            int answer = 0;

            for( String line : lines ) {
                System.out.println("line = " + line);
                String parts[] = line.split(":");
                String cards[] = parts[1].split("\\|");
                String winningNumbers[] = cards[0].split(" ");
                String cardNumbers[] = cards[1].split(" ");
                int matches = 0;
                for( String cardNumber : cardNumbers ) {
                    if( StringUtils.isNotBlank(cardNumber) && Arrays.asList(winningNumbers).contains(cardNumber) ) {
                        matches++;
                        System.out.println("MATCH cardNumber = " + cardNumber);
                    }
                }
                if( matches > 0 ){
                    System.out.println("matches = " + matches);
                    if( matches == 1 ) {
                        System.out.println("1:score = " + 1);
                        answer += 1;
                    } else {
                        Double score = Math.pow(2, matches-1);
                        System.out.println("score = " + score);
                        answer += score.intValue();
                    }

                }

            }
            return ""+answer;
        }

        if( isPart2() ) {
            int answer = 0;

            Map<Integer, List<Card>> cardMap = new HashMap<>();

            for( String line : lines ) {
//                System.out.println("line = " + line);
                String parts[] = line.split(":");
                Integer cardId = Integer.valueOf(parts[0].split(" ")[1]);
                String cardString[] = parts[1].split("\\|");
                String winningNumbers[] = cardString[0].split(" ");
                String cardNumbers[] = cardString[1].split(" ");
                Card card = new Card(cardId, winningNumbers, cardNumbers);
                List<Card> cards = new ArrayList<>();
                cards.add(card);
                cardMap.put(cardId, cards);
            }

            for (Map.Entry<Integer, List<Card>> entry : cardMap.entrySet()) {
                Integer key = entry.getKey();
                List<Card> cards = entry.getValue();

                System.out.println("Key: " + key);
                for (Card card : cards) {
                    int matches = 0;
                    for( String cardNumber : card.cardNumbers ) {
                        if( StringUtils.isNotBlank(cardNumber) && Arrays.asList(card.winningNumbers).contains(cardNumber) ) {
                            matches++;
//                            System.out.println("MATCH cardNumber = " + cardNumber);
                        }
                    }

                    for( int j = 1; j <= matches; j++) {
                        Integer keyToCopy = key+j;
                        if( cardMap.containsKey(keyToCopy) ) {
                            cardMap.get(keyToCopy).add(cardMap.get(keyToCopy).get(0).copy());
                        }
                    }

                }
            }
            answer = cardMap.entrySet().stream().map(e-> e.getValue().size()).reduce(0, Integer::sum);


            return ""+answer;
        }

        return "";
    }

    private class Card {

        private Integer cardId;
        private String winningNumbers[];
        private String cardNumbers[];

        public Card(Integer cardId, String winningNumbers[], String cardNumbers[]) {
            this.cardId = cardId;
            this.winningNumbers = winningNumbers;
            this.cardNumbers = cardNumbers;
        }

        public Card copy() {
            return new Card(this.cardId, this.winningNumbers, this.cardNumbers);
        }
    }



}
