package challenges;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import common.FileUtils;

import java.util.*;

public class Day1 {
    private static String INPUT_FILENAME = "/input/day1_input.txt";

    Map<String,Integer> numberMap = ImmutableMap.of(
            "one",1,
            "two",2,
            "three",3,
            "four",4,
            "five",5,
            "six",6,
            "seven",7,
            "eight",8,
            "nine",9
    );

    public String runChallenge(String inputFilename) throws Exception {

//        List<Integer> numbers = new ArrayList<>();
//
//        List<String> lines = FileUtils.getInputLines(inputFilename);
//        System.out.println("Input: lines.size() = " + lines.size());
//        for (String line : lines) {
//            //line 1abc2
//            //First numeric character
//            int firstNumericIndex = getFirstNumericIndex(line);
//            String firstDigit = line.charAt(firstNumericIndex) + "";
//
//            int lastNumericIndex = getLastNumericIndex(line);
//            String lastDigit = line.charAt(lastNumericIndex) + "";
//            numbers.add(Integer.valueOf(firstDigit+lastDigit));
//        }
//        //sum all numbers in list numbers
//        int sum = numbers.stream().reduce(0, Integer::sum);


        List<Integer> numbers = new ArrayList<>();


        List<String> lines = FileUtils.getInputLines(inputFilename);
        System.out.println("Input: lines.size() = " + lines.size());
        for (String line : lines) {
            System.out.println("line = " + line);
            int firstNumericIndex = getFirstNumericIndex(line);
            int firstStringIndex = getFirstStringIndex(line);

//            System.out.println("firstNumericIndex = " + firstNumericIndex);
//            System.out.println("firstStringIndex = " + firstStringIndex);
            String firstDigit = "";
            if( firstNumericIndex < firstStringIndex ) {
                firstDigit = line.charAt(firstNumericIndex) + "";
            } else {
                firstDigit = getFirstStringNumber(line) + "";
            }

            int lastNumericIndex = getLastNumericIndex(line);
            int lastStringIndex = getLastStringIndex(line);
            String lastDigit = "";
            if( lastNumericIndex > lastStringIndex ) {
                lastDigit = line.charAt(lastNumericIndex) + "";
            } else {
                lastDigit = getLastStringNumber(line) + "";
            }
            Integer number = Integer.valueOf(firstDigit+lastDigit);
            System.out.println("number = " + number);
            numbers.add(number);
        }
        int sum = numbers.stream().reduce(0, Integer::sum);
        System.out.println("sum = " + sum);
        return Integer.valueOf(sum).toString();
    }



    private int getLastNumericIndex(String line) {
        for (int i = line.length() - 1; i >= 0; i--) {
            if (Character.isDigit(line.charAt(i))) {
                return i;
            }
        }
        return -1;
    }

    private int getFirstNumericIndex(String line) {
        for (int i = 0; i < line.length(); i++) {
            if (Character.isDigit(line.charAt(i))) {
                return i;
            }
        }
        return 99;
    }

    private int getFirstStringIndex(String line) {
        int lowestIndex = 99;
        for (String key : numberMap.keySet()) {
            int index = line.indexOf(key);
            if (index != -1) {
                if (lowestIndex == -1) {
                    lowestIndex = index;
                } else {
                    lowestIndex = Math.min(lowestIndex, index);
                }
            }
        }
        return lowestIndex;
    }

    private int getLastStringIndex(String line) {
        int highestIndex = -1;
        for (String key : numberMap.keySet()) {
            int index = line.lastIndexOf(key);
            if (index != -1) {
                if (highestIndex == -1) {
                    highestIndex = index;
                } else {
                    highestIndex = Math.max(highestIndex, index);
                }
            }
        }
        return highestIndex;
    }

//    private int getLastStringNumber(String line) {
//        int lastStringIndex = getLastStringIndex(line);
//        String substring = line.substring(lastStringIndex);
//        String nmberKey = "";
//        for (String key : numberMap.keySet()) {
//            if (substring.contains(key)) {
//                return numberMap.get(key);
//            }
//        }
//        return 0;
//    }

    private int getLastStringNumber(String line) {
        int highestIndex = -1;
        int result = 0;
        for (String key : numberMap.keySet()) {
            int index = line.lastIndexOf(key);
            if (index != -1) {
                if (highestIndex == -1) {
                    highestIndex = index;
                    result = numberMap.get(key);
                } else {
                    if( index > highestIndex ) {
                        result = numberMap.get(key);
                    }
                    highestIndex = Math.max(highestIndex, index);
                }
            }
        }
        return result;
    }


//    private int getFirstStringNumber(String line) {
//        int firstStringIndex = getFirstStringIndex(line);
//        String substring = line.substring(firstStringIndex,firstStringIndex+5);
//        String nmberKey = "";
//        for (String key : numberMap.keySet()) {
//            if (substring.contains(key)) {
//                return numberMap.get(key);
//            }
//        }
//        return 0;
//    }

    private int getFirstStringNumber(String line) {
        int lowestIndex = -1;
        int result = 0;
        for (String key : numberMap.keySet()) {
            int index = line.indexOf(key);
            if (index != -1) {
                if (lowestIndex == -1) {
                    lowestIndex = index;
                    result = numberMap.get(key);
                } else {
                    if( index < lowestIndex ) {
                        result = numberMap.get(key);
                    }
                    lowestIndex = Math.min(lowestIndex, index);
                }
            }
        }
        return result;
    }

}

