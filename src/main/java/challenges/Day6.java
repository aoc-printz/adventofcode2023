package challenges;

import common.AdventOfCodeBase;
import common.Part;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;


public class Day6 extends AdventOfCodeBase {

    public Day6(String inputFilename, Part part) {
        super(inputFilename, part);
    }

    @Override
    public String run() {
        System.out.println("INPUT lines.size() = " + lines.size());

        if( isPart1()) {
            int answer = 0;

            List<Integer> times = new ArrayList<>();
            List<Integer> distances = new ArrayList<>();
            List<Integer> results = new ArrayList<>();

            for( String line : lines ) {
                System.out.println("line = " + line);
                String parts[] = line.split(":");
                if( parts[0].equals("Time")) {
                    String numbers[] = parts[1].split(" ");
                    for( String number : numbers ) {
                        System.out.println("time = " + number);
                        if(StringUtils.isNotBlank(number)) {
                            times.add(Integer.valueOf(number));
                        }
                    }
                } else {
                    String numbers[] = parts[1].split(" ");
                    for( String number : numbers ) {
                        System.out.println("distance = " + number);
                        if(StringUtils.isNotBlank(number)) {
                            distances.add(Integer.valueOf(number));
                        }
                    }
                }
            }

            int position = 0;
            for( Integer time : times ) {
                System.out.println("time = " + time);
                Integer distance = distances.get(position);

                int validSolutions = 0;
                for( int i = 0; i < time; i++ ) {
                    int speed = i;
                    int distanceTraveled = speed * (time - i);

                    if( distanceTraveled > distance ) {
                        System.out.println("valid");
                        validSolutions++;
                    }
                }
                results.add(validSolutions);
                position++;
            }

            for( Integer result : results ) {
                System.out.println("result = " + result);
                if( answer == 0) {
                    answer = result;
                } else {
                    answer = answer * result;
                }
            }

            return ""+answer;
        }

        if( isPart2() ) {
            int answer = 0;

            Double time = Double.valueOf(lines.get(0).replaceAll(" ","").split(":")[1]);
            Double distance = Double.valueOf(lines.get(1).replaceAll(" ","").split(":")[1]);
            System.out.println("time = " + time);
            System.out.println("distance = " + distance);

            double validSolutions = 0;
            for( double i = 0; i < time; i++ ) {
                Double speed = i;
                Double distanceTraveled = speed * (time - i);

                if( distanceTraveled > distance ) {
                    validSolutions++;
                }
            }

            try {
                System.out.println("answer = " + String.format("%.2f", validSolutions));
            } catch (Exception e) {
                System.out.println("answer = " + validSolutions);
            }

            return ""+validSolutions;
        }

        return "";
    }


}
