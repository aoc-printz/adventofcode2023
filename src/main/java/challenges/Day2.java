package challenges;

import com.google.common.collect.ImmutableMap;
import common.AdventOfCodeBase;
import common.Part;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


public class Day2 extends AdventOfCodeBase {
    Map<String,Integer> maxPerColor = ImmutableMap.of(
            "red",12,
            "green",13,
            "blue",14
    );

    public Day2(String inputFilename, Part part) {
        super(inputFilename, part);
    }

    @Override
    public String run() {
        System.out.println("INPUT lines.size() = " + lines.size());
        if( isPart1()) {
            int sumOfGameIds = 0;

            for (String line : lines) {
                System.out.println("line = " + line);
                String parts[] = line.split(":");
                String gameId = parts[0];
                int gameIdInt = Integer.valueOf(gameId.split(" ")[1]);
                String allGames = parts[1];
                String games[] = allGames.split(";");
                List<String> gameList = new ArrayList<>(Arrays.asList(games));
                boolean isGameValid = true;
                for (String game : gameList) {
                    String gameParts[] = game.split(",");
                    List<String> gamePartList = new ArrayList<>(Arrays.asList(gameParts));
                    for (String gamePart : gamePartList) {
                        String gamePartParts[] = gamePart.trim().split(" ");
                        String number = gamePartParts[0];
                        String color = gamePartParts[1];
                        System.out.println("color = " + color);
                        System.out.println("number = " + number);
                        if (Integer.valueOf(number) > maxPerColor.get(color)) {
                            System.out.println("INVALID GAME");
                            isGameValid = false;
                            break;
                        }
                    }
                }
                if (isGameValid) {
                    sumOfGameIds += gameIdInt;
                }
            }
            return ""+sumOfGameIds;
        }

        if( isPart2() ) {
            int validGamesCount = 0;
            int sumOfPowers = 0;

            for (String line : lines) {
                System.out.println("line = " + line);
                String parts[] = line.split(":");
                String gameId = parts[0];
                int gameIdInt = Integer.valueOf(gameId.split(" ")[1]);
                String allGames = parts[1];
                String games[] = allGames.split(";");
                List<String> gameList = new ArrayList<>(Arrays.asList(games));

                int minBlue = 1;
                int minGreen = 1;
                int minRed = 1;


                for (String game : gameList) {
                    System.out.println("game = " + game);

                    String gameParts[] = game.split(",");
                    List<String> gamePartList = new ArrayList<>(Arrays.asList(gameParts));
                    for (String gamePart : gamePartList) {
                        System.out.println("gamePart = " + gamePart);
                        String gamePartParts[] = gamePart.trim().split(" ");
                        String number = gamePartParts[0];
                        String color = gamePartParts[1];
//                        System.out.println("color = " + color);
//                        System.out.println("number = " + number);
                        if (color.equals("blue")) {
                            if (Integer.valueOf(number) > minBlue) {
                                minBlue = Integer.valueOf(number);
                            }
                        }
                        if (color.equals("green")) {
                            if (Integer.valueOf(number) > minGreen) {
                                minGreen = Integer.valueOf(number);
                            }
                        }
                        if (color.equals("red")) {
                            if (Integer.valueOf(number) > minRed) {
                                minRed = Integer.valueOf(number);
                            }
                        }
                    }
                }
                System.out.println("minRed = " + minRed);
                System.out.println("minBlue = " + minBlue);
                System.out.println("minGreen = " + minGreen);
                sumOfPowers+= minBlue * minGreen * minRed;
            }
            return ""+sumOfPowers;
        }

        return "";
    }

}
