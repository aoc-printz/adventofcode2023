package challenges;

import common.AdventOfCodeBase;
import common.Part;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Day3 extends AdventOfCodeBase {

    public Day3(String inputFilename, Part part) {
        super(inputFilename, part);
    }

    @Override
    public String run() {
        System.out.println("INPUT lines.size() = " + lines.size());
        String[][] grid = new String[lines.size()][lines.get(0).length()];
        for (int i = 0; i < lines.size(); i++) {
            grid[i] = lines.get(i).split("");
        }

        if( isPart1()) {

            List<Integer> partNumbers = new ArrayList<>();
            for( int i = 0; i < grid.length; i++ ) {
                for( int j = 0; j < grid[i].length; j++ ) {
                    System.out.println("Char:" + grid[i][j]);
                    String character = grid[i][j];
                    if(StringUtils.isNumeric(character)) {
                        Integer number = getNumberFromGridStartingAt(grid, i, j);
                        System.out.println("number = " + number);
                        int numberLength = number.toString().length();
                        boolean hasAdjactantSymbol = false;
                        for( int k = 0; k < numberLength; k++ ) {
                            if( gridHasAdjactantSymbol(grid, i, j+k) ) {
                                hasAdjactantSymbol = true;
                                break;
                            }
                        }
                        if( hasAdjactantSymbol ) {
                            partNumbers.add(number);
                            j+=numberLength-1;
                        }
                    }


                }
            }
            int result = partNumbers.stream().mapToInt(Integer::intValue).sum();
            return ""+result;
        }

        if( isPart2() ) {
            List<Integer> gearNumbers = new ArrayList<>();
            for( int i = 0; i < grid.length; i++ ) {
                for (int j = 0; j < grid[i].length; j++) {
                    System.out.println("Char:" + grid[i][j]);
                    String character = grid[i][j];
                    if( "*".equals(character) ) {
                        Integer product = gridProductOfTwoAdjacentNumbers(grid, i, j);
                        gearNumbers.add(product);
                    }
                }
            }
            int result = gearNumbers.stream().mapToInt(Integer::intValue).sum();
            return ""+result;
        }

        return "";
    }

    private Integer getNumberFromGridStartingAt( String[][] grid, int row, int col ) {
        List<String> number = new ArrayList<>();
        for( int i = row; i < grid.length; i++ ) {
            for( int j = col; j < grid[i].length; j++ ) {
                String character = grid[i][j];
                if(StringUtils.isNumeric(character)) {
                    number.add(character);
                }
                else {
                    return Integer.valueOf(StringUtils.join(number, ""));
                }
            }
        }
        return Integer.valueOf(StringUtils.join(number, ""));
    }

    private boolean gridHasAdjactantSymbol( String[][] grid, int row, int col ) {
        if( row > 0 ) {
            //Up
            if( isSymbol(grid[row-1][col]) ) {
                return true;
            }
            //Up-Left
            if( col > 0 && isSymbol(grid[row-1][col-1]) ) {
                return true;
            }
            //Up-Right
            if( col < grid[row].length-1 && isSymbol(grid[row-1][col+1]) ) {
                return true;
            }
        }
        if( row < grid.length -1 ) {
            //Down
            if( isSymbol(grid[row+1][col]) ) {
                return true;
            }
            //Down-Left
            if( col > 0 && isSymbol(grid[row+1][col-1]) ) {
                return true;
            }
            //Down-Right
            if( col < grid[row].length-1 && isSymbol(grid[row+1][col+1]) ) {
                return true;
            }
        }
        //Left
        if (col > 0 && isSymbol(grid[row][col - 1])) {
            return true;
        }
        //Right
        if (col < grid[row].length-1 && isSymbol(grid[row][col + 1])) {
            return true;
        }
        return false;
    }

    private Integer gridProductOfTwoAdjacentNumbers( String[][] grid, int row, int col ) {
        List<Integer> adjacentNumbers = new ArrayList<>();
        if( row > 0 ) {
            //Up-Left
            if( col > 0 && StringUtils.isNumeric(grid[row-1][col-1]) ) {
                Integer number = getNumberFromGridPosition(grid, row-1, col-1);
                adjacentNumbers.add(number);
            }

            //Up
            if( StringUtils.isNumeric(grid[row-1][col]) ) {
                Integer number = getNumberFromGridPosition(grid, row-1, col);
                adjacentNumbers.add(number);
            }

            //Up-Right
            if( col < grid[row].length-1 && StringUtils.isNumeric(grid[row-1][col+1]) ) {
                Integer number = getNumberFromGridPosition(grid, row-1, col+1);
                adjacentNumbers.add(number);
            }
        }
        if( row < grid.length -1 ) {
            //Down
            if( StringUtils.isNumeric(grid[row+1][col]) ) {
                Integer number = getNumberFromGridPosition(grid, row+1, col);
                adjacentNumbers.add(number);
            }
            //Down-Left
            if( col > 0 && StringUtils.isNumeric(grid[row+1][col-1]) ) {
                Integer number = getNumberFromGridPosition(grid, row+1, col-1);
                adjacentNumbers.add(number);
            }
            //Down-Right
            if( col < grid[row].length-1 && StringUtils.isNumeric(grid[row+1][col+1]) ) {
                Integer number = getNumberFromGridPosition(grid, row+1, col+1);
                adjacentNumbers.add(number);
            }
        }
        //Left
        if (col > 0 && StringUtils.isNumeric(grid[row][col - 1])) {
            Integer number = getNumberFromGridPosition(grid, row, col-1);
            adjacentNumbers.add(number);
        }
        //Right
        if (col < grid[row].length-1 && StringUtils.isNumeric(grid[row][col + 1])) {
            Integer number = getNumberFromGridPosition(grid, row, col+1);
            adjacentNumbers.add(number);
        }
        //Product
        List<Integer> gearNumbers = adjacentNumbers.stream().distinct().toList();
        if( gearNumbers.size() == 2 ) {
            return gearNumbers.get(0).intValue() * gearNumbers.get(1).intValue();
        }
        return 0;
    }

    private Integer getNumberFromGridPosition( String[][] grid, int row, int col ) {
        int numberStart = col;
        for( int j = col; j >= 0; j-- ) {
            String character = grid[row][j];
            if( StringUtils.isNumeric(character)) {
                numberStart = j;
            } else {
                break;
            }
        }
        int numberEnd = 0;
        for( int j = col; j < grid[row].length; j++ ) {
            String character = grid[row][j];
            if( StringUtils.isNumeric(character)) {
                numberEnd = j;
            } else {
                break;
            }
        }
        return Integer.valueOf(StringUtils.join(Arrays.copyOfRange(grid[row], numberStart, numberEnd+1), ""));
    }

    private boolean isSymbol(String character) {
        return !StringUtils.isNumeric(character) && !character.equals(".");
    }

}
