package challenges;

import common.AdventOfCodeBase;
import common.Part;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


public class Day7 extends AdventOfCodeBase {

    public Day7(String inputFilename, Part part) {
        super(inputFilename, part);
    }

    @Override
    public String run() {
        List<Hand> hands = new ArrayList<>();

        for( String line : lines ) {
            String parts[] = line.split(" ");
            hands.add(new Hand(parts[0], Integer.parseInt(parts[1])));
        }

        Collections.sort(hands);
        long answer = 0;
        for (int i = 0; i < hands.size(); i++) {
            answer += hands.get(i).bid * (i + 1);
        }
        return answer + "";
    }

    //Help from https://github.com/abnew123/aoc2023/blob/main/src/solutions/Day07.java
    class Hand implements Comparable {
        int bid;
        int strength;
        int[] freqs = new int[13];
        int[] cards = new int[5];
        String[] ranks= new String[]{"A", "K", "Q", "J", "T", "9", "8", "7", "6", "5", "4", "3", "2"};

        public Hand(String line, int bid) {
            if (isPart2()) {
                ranks = new String[]{"A", "K", "Q", "T", "9", "8", "7", "6", "5", "4", "3", "2", "J"};
            }
            this.bid = bid;
            int numJokers = 0;

            String[] s = line.split("");
            for (int i = 0; i < s.length; i++) {
                for (int j = 0; j < ranks.length; j++) {
                    String character = s[i];
                    if (character.equals(ranks[j])) {
                        if (!character.equals("J") || isPart1()) {
                            freqs[j]++;
                        }
                        if (character.equals("J") && isPart2()) {
                            numJokers++;
                        }
                        cards[i] = j;
                    }
                }
            }
            Arrays.sort(freqs);
            freqs[freqs.length - 1] += numJokers;
            strength = 2 * freqs[freqs.length - 1];
            if (freqs[freqs.length - 2] == 2) {
                strength += 1; //for full house and two pair
            }
        }

        @Override
        public int compareTo(Object o) {
            Hand other = (Hand) o;
            if (strength != other.strength) {
                return strength - other.strength;
            } else {
                for (int i = 0; i < cards.length; i++) {
                    if (cards[i] != other.cards[i]) {
                        return other.cards[i] - cards[i];
                    }
                }
                return 0;
            }
        }
    }

}
