package challenges;

import common.AdventOfCodeBase;
import common.Part;
import org.apache.commons.lang3.StringUtils;

import java.util.*;


public class Day5 extends AdventOfCodeBase {

    public Day5(String inputFilename, Part part) {
        super(inputFilename, part);
    }

    @Override
    public String run() {
        System.out.println("INPUT lines.size() = " + lines.size());

        if( isPart1()) {
            List<Double> seeds = new ArrayList<>();
            String seedString = lines.get(0);
            String seedStrings[] = seedString.split(":");
            String seedNumbers[] = seedStrings[1].trim().split(" ");
            for( String seedNumber : seedNumbers ) {
                seeds.add(Double.valueOf(seedNumber));
            }

            List<AlmanacMap> seedToSoil = new ArrayList<>();
            List<AlmanacMap> soilToFertilizer = new ArrayList<>();
            List<AlmanacMap> fertilizerToWater = new ArrayList<>();
            List<AlmanacMap> waterToLight = new ArrayList<>();
            List<AlmanacMap> lightToTemperature = new ArrayList<>();
            List<AlmanacMap> temperatureToHumidity = new ArrayList<>();
            List<AlmanacMap> humidityToLocation = new ArrayList<>();

            Map<String,List<AlmanacMap>> allMaps = Map.of(
                    "seed-to-soil", seedToSoil,
                    "soil-to-fertilizer",soilToFertilizer,
                    "fertilizer-to-water",fertilizerToWater,
                    "water-to-light",waterToLight,
                    "light-to-temperature",lightToTemperature,
                    "temperature-to-humidity",temperatureToHumidity,
                    "humidity-to-location",humidityToLocation
            );

            List<AlmanacMap> currentMap = new ArrayList<>();

            for( int i = 1; i < lines.size(); i++) {
                String line = lines.get(i);
                System.out.println("line = " + line);
                if( line.trim().isEmpty() ) {
                    continue;
                }

                if( !StringUtils.isNumeric(line.charAt(0)+"") ) {
                    String parts[] = line.split(" ");
                    currentMap = allMaps.get(parts[0]);
                } else {
                    String parts[] = line.split(" ");
                    double destinationRangeStart = Double.valueOf(parts[0]);
                    double sourceRangeStart = Double.valueOf(parts[1]);
                    double rangeLength = Double.valueOf(parts[2]);
                    currentMap.add(new AlmanacMap(destinationRangeStart,sourceRangeStart,rangeLength));
                }

            }

            Double answer = null;

            for( Double seed : seeds ) {
                double soil = getLocation(seed, seedToSoil);
                double fertilizer = getLocation(soil, soilToFertilizer);
                double water = getLocation(fertilizer, fertilizerToWater);
                double light = getLocation(water, waterToLight);
                double temperature = getLocation(light, lightToTemperature);
                double humidity = getLocation(temperature, temperatureToHumidity);
                double location = getLocation(humidity, humidityToLocation);
                try {
                    System.out.println("location = " + String.format("%.2f", location));
                } catch (Exception e) {
                    System.out.println("location = " + location);
                }

                if( answer == null || location < answer ) {
                    answer = location;
                }

                try {
                    System.out.println("answer = " + String.format("%.2f", answer));
                } catch (Exception e) {
                    System.out.println("answer = " + answer);
                }
            }

            return ""+answer;
        }

        if( isPart2() ) {
            List<Double> seeds = new ArrayList<>();
            String seedString = lines.get(0);
            String seedStrings[] = seedString.split(":");
            String seedNumbers[] = seedStrings[1].trim().split(" ");
            for( String seedNumber : seedNumbers ) {
                seeds.add(Double.valueOf(seedNumber));
            }

            List<AlmanacMap> seedToSoil = new ArrayList<>();
            List<AlmanacMap> soilToFertilizer = new ArrayList<>();
            List<AlmanacMap> fertilizerToWater = new ArrayList<>();
            List<AlmanacMap> waterToLight = new ArrayList<>();
            List<AlmanacMap> lightToTemperature = new ArrayList<>();
            List<AlmanacMap> temperatureToHumidity = new ArrayList<>();
            List<AlmanacMap> humidityToLocation = new ArrayList<>();

            Map<String,List<AlmanacMap>> allMaps = Map.of(
                    "seed-to-soil", seedToSoil,
                    "soil-to-fertilizer",soilToFertilizer,
                    "fertilizer-to-water",fertilizerToWater,
                    "water-to-light",waterToLight,
                    "light-to-temperature",lightToTemperature,
                    "temperature-to-humidity",temperatureToHumidity,
                    "humidity-to-location",humidityToLocation
            );

            List<AlmanacMap> currentMap = new ArrayList<>();

            for( int i = 1; i < lines.size(); i++) {
                String line = lines.get(i);
                System.out.println("line = " + line);
                if( line.trim().isEmpty() ) {
                    continue;
                }
                //if first character is a number
                if( !StringUtils.isNumeric(line.charAt(0)+"") ) {
                    String parts[] = line.split(" ");
                    currentMap = allMaps.get(parts[0]);
                } else {
                    String parts[] = line.split(" ");
                    double destinationRangeStart = Double.valueOf(parts[0]);
                    double sourceRangeStart = Double.valueOf(parts[1]);
                    double rangeLength = Double.valueOf(parts[2]);
                    currentMap.add(new AlmanacMap(destinationRangeStart,sourceRangeStart,rangeLength));
                }

            }

            //Logic: by looking at the data file, in the humidity-to-location section,
            // there is a line that has a destinationRangeStart of 0
            // I assumed that this line would produce the smallest possible location
            // And used the rangeLenth of that line as the answer
            AlmanacMap lowestPossibleLocation = humidityToLocation.stream().min(Comparator.comparingDouble(o -> o.destinationRangeStart)).get();
            Double answer =  lowestPossibleLocation.rangeLength;
            try {
                System.out.println("answer = " + String.format("%.2f", answer));
            } catch (Exception e) {
                System.out.println("answer = " + answer);
            }
            return ""+answer;
        }

        return "";
    }

    private Double getLocation(Double seed, List<AlmanacMap> maps) {
        for( AlmanacMap map : maps ) {
            if( seed >= map.destinationSourceStart && seed < map.destinationSourceStart+map.rangeLength ) {
                return map.destinationRangeStart + (seed-map.destinationSourceStart);
            }
        }
        return seed;
    }

    public class AlmanacMap {
        private Double destinationRangeStart;
        private Double destinationSourceStart;
        private Double rangeLength;

        public AlmanacMap(Double destinationRangeStart, Double destinationSourceStart, Double rangeLength) {
            this.destinationRangeStart = destinationRangeStart;
            this.destinationSourceStart = destinationSourceStart;
            this.rangeLength = rangeLength;
        }
    }


}
