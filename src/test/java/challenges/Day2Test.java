package challenges;

import challenges.Day2;
import common.Part;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day2Test {

    private static final String INPUT = "/input/day2_input.txt";
    private static final String SAMPLE_INPUT = "/input/day2_sample_input.txt";


    @Test
    void run_part1_sample() {
        Day2 day = new Day2(SAMPLE_INPUT, Part.PART_ONE);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(8, Integer.valueOf(result));
    }

    @Test
    void run_part1() {
        Day2 day = new Day2(INPUT, Part.PART_ONE);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(2156, Integer.valueOf(result));
    }

    @Test
    void run_part2_sample() {
        Day2 day = new Day2(SAMPLE_INPUT, Part.PART_TWO);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(2286, Integer.valueOf(result));
    }

    @Test
    void run_part2() {
        Day2 day = new Day2(INPUT, Part.PART_TWO);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(0, Integer.valueOf(result));
    }

}