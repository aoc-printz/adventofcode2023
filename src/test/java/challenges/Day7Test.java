package challenges;

import common.Part;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day7Test {

    private static final String INPUT = "/input/day7_input.txt";
    private static final String SAMPLE_INPUT = "/input/day7_sample_input.txt";


    @Test
    void run_part1_sample() {
        Day7 day = new Day7(SAMPLE_INPUT, Part.PART_ONE);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(6440.0, Double.valueOf(result));
    }

    @Test
    void run_part1() {
        Day7 day = new Day7(INPUT, Part.PART_ONE);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(251806792.0, Double.valueOf(result));

        //251806944
        //251806944
    }

    @Test
    void run_part2_sample() {
        Day7 day = new Day7(SAMPLE_INPUT, Part.PART_TWO);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(5905.0, Double.valueOf(result));
    }

    @Test
    void run_part2() {
        Day7 day = new Day7(INPUT, Part.PART_TWO);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(252113488.0, Double.valueOf(result));
        //Try 249771344
        //250124081
        //249960578
        //252113488
    }

}