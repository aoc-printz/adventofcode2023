package challenges;

import common.Part;
import challenges.Day3;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day3Test {

    private static final String INPUT = "/input/day3_input.txt";
    private static final String SAMPLE_INPUT = "/input/day3_sample_input.txt";


    @Test
    void run_part1_sample() {
        Day3 day = new Day3(SAMPLE_INPUT, Part.PART_ONE);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(4361, Integer.valueOf(result));
    }

    @Test
    void run_part1() {
        Day3 day = new Day3(INPUT, Part.PART_ONE);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(536576, Integer.valueOf(result));
    }

    @Test
    void run_part2_sample() {
        Day3 day = new Day3(SAMPLE_INPUT, Part.PART_TWO);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(467835, Integer.valueOf(result));
    }

    @Test
    void run_part2() {
        Day3 day = new Day3(INPUT, Part.PART_TWO);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(75741499, Integer.valueOf(result));
    }

}