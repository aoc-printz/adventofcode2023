package challenges;

import common.Part;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day4Test {

    private static final String INPUT = "/input/day4_input.txt";
    private static final String SAMPLE_INPUT = "/input/day4_sample_input.txt";


    @Test
    void run_part1_sample() {
        Day4 day = new Day4(SAMPLE_INPUT, Part.PART_ONE);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(13, Integer.valueOf(result));
    }

    @Test
    void run_part1() {
        Day4 day = new Day4(INPUT, Part.PART_ONE);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(15205, Integer.valueOf(result));
    }

    @Test
    void run_part2_sample() {
        Day4 day = new Day4(SAMPLE_INPUT, Part.PART_TWO);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(30, Integer.valueOf(result));
    }

    @Test
    void run_part2() {
        Day4 day = new Day4(INPUT, Part.PART_TWO);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(6189740, Integer.valueOf(result));
    }

}