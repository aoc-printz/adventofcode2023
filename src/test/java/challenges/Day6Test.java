package challenges;

import common.Part;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day6Test {

    private static final String INPUT = "/input/day6_input.txt";
    private static final String SAMPLE_INPUT = "/input/day6_sample_input.txt";


    @Test
    void run_part1_sample() {
        Day6 day = new Day6(SAMPLE_INPUT, Part.PART_ONE);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(288, Integer.valueOf(result));
    }

    @Test
    void run_part1() {
        Day6 day = new Day6(INPUT, Part.PART_ONE);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(1624896, Integer.valueOf(result));
    }

    @Test
    void run_part2_sample() {
        Day6 day = new Day6(SAMPLE_INPUT, Part.PART_TWO);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(71503.0, Double.valueOf(result));
    }

    @Test
    void run_part2() {
        Day6 day = new Day6(INPUT, Part.PART_TWO);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(32583852.0, Double.valueOf(result));
    }

}