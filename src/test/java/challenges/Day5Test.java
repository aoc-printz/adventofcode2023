package challenges;

import common.Part;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day5Test {

    private static final String INPUT = "/input/day5_input.txt";
    private static final String SAMPLE_INPUT = "/input/day5_sample_input.txt";


    @Test
    void run_part1_sample() {
        Day5 day = new Day5(SAMPLE_INPUT, Part.PART_ONE);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(35.0, Double.valueOf(result));
    }

    @Test
    void run_part1() {
        Day5 day = new Day5(INPUT, Part.PART_ONE);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(165788812.00, Double.valueOf(result));
    }

    @Test
    void run_part2_sample() {
        //Did not use
//        Day5 day = new Day5(SAMPLE_INPUT, Part.PART_TWO);
//        String result = day.run();
//        System.out.println("result= " + result);
//        assertEquals(46.0, Double.valueOf(result));
    }

    @Test
    void run_part2() {
        Day5 day = new Day5(INPUT, Part.PART_TWO);
        String result = day.run();
        System.out.println("result= " + result);
        assertEquals(1928058, Double.valueOf(result));
    }

}