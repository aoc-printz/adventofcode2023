package challenges;

import challenges.Day1;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Day1Test {

    private Day1 day1 = new Day1();

    @Test
    void runChallenge() throws Exception {
        String result = day1.runChallenge("/input/day1_input.txt");
        System.out.println("result = " + result);
        //47161
    }

//    @Test
//    void runChallengeSample() throws Exception {
//        String result = day1.runChallenge("/input/day1_sample_input.txt");
//        System.out.println("result = " + result);
//        assertEquals(result, "142");
//    }

    @Test
    void runChallengeSamplePart2() throws Exception {
        String result = day1.runChallenge("/input/day1_sample_input.txt");
        System.out.println("result = " + result);
        assertEquals(result, "281");
    }

}